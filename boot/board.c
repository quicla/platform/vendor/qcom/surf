/* Copyright 2007, Google Inc. */

#include <boot/boot.h>
#include <boot/flash.h>
#include <msm7k/shared.h>

ptentry PTABLE[] = {
    {
        .start = 298,
        .length = 40,
        .name = "boot",
    },
    {
        .start = 354,
        .length = 512,
        .name = "system",
    },
    {
        .start = 866,
        .length = 157 + 1024,
        .name = "userdata",
    },
    {
        .name = "",
    },
};

const char *board_cmdline(void) 
{
    return "mem=112M androidboot.console=ttyMSM0 console=ttyMSM0";
}

unsigned board_machtype(void)
{
    return 1439;
}

void board_init()
{
    unsigned n;
    
    /* if we already have partitions from elsewhere,
    ** don't use the hardcoded ones
    */
    if(flash_get_ptn_count() == 0) {
        for(n = 0; PTABLE[n].name[0]; n++) {
            flash_add_ptn(PTABLE + n);
        }
    }

    clock_enable(UART1_CLK);
    clock_set_rate(UART1_CLK, 19200000 / 4);

    uart_init(0);
}

void board_usb_init(void)
{
}

void board_ulpi_init(void)
{
}

void board_reboot(void)
{
}

void board_getvar(const char *name, char *value)
{
}
